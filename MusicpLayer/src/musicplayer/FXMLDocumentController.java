/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package musicplayer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Map;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 *
 * @author valentin
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button file;
    @FXML
    private Button parameters;
    @FXML
    private Button mute;
    @FXML
    private Button random;
    @FXML
    private Button previous;
    @FXML
    private Button rearward;
    @FXML
    private Button play;
    @FXML
    private Button forward;
    @FXML
    private Button next;
    @FXML
    private Button pause;
    @FXML
    private Slider soundBar;
    @FXML
    private Slider timeBar;
    @FXML
    private Label LabelMusic;
    @FXML
    private Label actualDuration;
    @FXML
    private Label totalDuration;
    @FXML
    private ImageView atwork;
    @FXML
    private ImageView muteImg;
    @FXML
    private ImageView randomImg;
    @FXML
    private TableView<Music> musicList = new TableView<Music>();;
    @FXML
    private TableColumn<Music, String> musicNameColumn;
    @FXML
    private TableColumn<Music, String> musicAuthorColumn;
    @FXML
    private TableColumn<Music, String> musicTimeColumn;
    @FXML
    private TableColumn<Music, String> musicExtensionColumn;
    @FXML
    private TableColumn<Music, String> musicAlbumColumn;
    @FXML
    private TableColumn<Music, String> musicGenreColumn;
    @FXML
    private TableColumn<Music, String> musicYearColumn;
    
    private ObservableList<Music> data;
    private MediaPlayer mediaPlayer;
    private Media sound;
    private Duration musicDuration;
    private boolean isRandom = false;



            
    @Override
    public void initialize(URL url, ResourceBundle rb) {      
        
        musicNameColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicName"));
        
        musicAuthorColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicAuthor"));
        
        musicTimeColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicTime"));
        
        musicExtensionColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicExtension"));
        
        musicAlbumColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicAlbum"));
        
        musicGenreColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicGenre"));
        
        musicYearColumn.setCellValueFactory(
            new PropertyValueFactory<>("musicYear"));
    
        file.setOnAction((event) -> this.fileChooser(event));
        next.setOnAction((event) -> this.next(event));
        play.setOnAction((event) -> this.play(event));
        previous.setOnAction((event) -> this.previous(event));
        forward.setOnAction((event) -> this.forward(event));
        rearward.setOnAction((event) -> this.rearward(event));
        mute.setOnAction((event) -> this.mute(event)); 
        random.setOnAction((event) -> this.random(event));
        pause.setOnAction((event) -> this.pause(event));
        parameters.setOnAction((event) -> this.parameters(event));
        
        Map<String, String> env = System.getenv();
        String userPath = "";
        for (String envName : env.keySet()) {
            switch (envName) {
                case "USERPROFILE":
                    userPath = env.get(envName) + "\\Music";
                    break;
                case "XDG_MUSIC_DIR":
                    userPath = env.get(envName);
                    break;
                case "HOME":
                    userPath = env.get(envName) + "/Music";
                    break;
                default:
                    break;
            }
        }
        
        File ifile = new File(userPath);
        File[] files = ifile.listFiles();
        if (files != null){
            for (File file1 : files) {
                if (file1.isFile() && file1.getAbsolutePath().substring(file1.getAbsolutePath().lastIndexOf(".") + 1).equals("mp3") || 
                    file1.getAbsolutePath().substring(file1.getAbsolutePath().lastIndexOf(".") + 1).equals("wav")) {
                    data = musicList.getItems();
                    Music test = new Music(file1);
                    data.add(test);
                }
            }
        }   
        
        soundBar.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
            boolean isMute = false;
            if (mediaPlayer != null) {
                mediaPlayer.setVolume(newValue.doubleValue());
                if (mediaPlayer.isMute()) {
                    isMute = true;
                }
            }
            if (isMute == false) {
                setImgSound();
            }
        });
        
        timeBar.valueProperty().addListener((ObservableValue<? extends Number> ov, Number oldValue, Number newValue) -> {
            if (timeBar.isValueChanging()) {
                if (mediaPlayer != null)
                    mediaPlayer.seek(musicDuration.multiply(timeBar.valueProperty().getValue() / 100.0));
                else
                    timeBar.valueProperty().setValue(0);
            }
            else if (timeBar.valueProperty().getValue() == 100)
                    next(null);
        });
        
        musicList.setRowFactory(tv -> {
            TableRow<Music> selectedMusic = new TableRow<>();
            selectedMusic.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !selectedMusic.isEmpty()) {
                    Music rowData = selectedMusic.getItem();
                    play(null);
                }
            });
            return selectedMusic;
        });
    }
    
    public void mute(ActionEvent event) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isMute()) {
                mediaPlayer.setMute(false);
                setImgSound();
            }
            else {
                mediaPlayer.setMute(true);
                muteImg.setImage(new Image(getClass().getResourceAsStream("img/mute.png")));
            }
        }
    }  
    
    public void random(ActionEvent event) {        
        if (isRandom == true)
        {
            isRandom = false;
            randomImg.setImage(new Image(getClass().getResourceAsStream("img/repeat.png")));            
        }
        else
        {
            isRandom = true;
            randomImg.setImage(new Image(getClass().getResourceAsStream("img/shuffle.png")));            
        }
    }
    
    public void next(ActionEvent event) {
        if (isRandom == false){
            if (mediaPlayer != null) {
                if (musicList.getSelectionModel().getSelectedIndex() == musicList.getItems().size() - 1)
                    musicList.getSelectionModel().selectFirst();
                else{
                    musicList.getSelectionModel().selectNext();
                }
                play(null);
            }
        }
        else if (isRandom == true){
            if (mediaPlayer != null) {
                Random Dice = new Random();
                int rand = Dice.nextInt(musicList.getItems().size());
                musicList.getSelectionModel().select(rand);
            }
            play(null);
        }
    }
    
    public void previous(ActionEvent event) {
        if (mediaPlayer != null) {
            if (musicList.getSelectionModel().getSelectedIndex() == 0)
                musicList.getSelectionModel().selectLast();
            else
                musicList.getSelectionModel().selectPrevious();
            play(null);
        }       
    }
    
    public void fileChooser(ActionEvent event) {
        final FileChooser fd = new FileChooser();
        final FileChooser.ExtensionFilter imgFilter = new FileChooser.ExtensionFilter("Musique *.mp3, *.wav", "*.mp3", "*.wav");
        fd.getExtensionFilters().setAll(imgFilter);
        fd.setSelectedExtensionFilter(imgFilter);
        final File sfile = fd.showOpenDialog(file.getScene().getWindow());           
        if (sfile.isFile() && !sfile.isDirectory()){
            data = musicList.getItems();
            Music test = new Music(sfile);
            data.add(test);
        }
    } 
        
    public void pause(ActionEvent event) {        
        if (mediaPlayer != null) {
            if (mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED))
                mediaPlayer.play();
            else {
                mediaPlayer.pause();
            }
            
            mediaPlayer.currentTimeProperty().addListener((ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) -> {
                if (mediaPlayer != null) {
                    timeBar.valueProperty().setValue(newValue.divide(musicDuration.toMillis()).toMillis() * 100.0);
                    int minutes = (int) (newValue.toSeconds() / 60);
                    int seconds = (int) (newValue.toSeconds() - (minutes * 60));
                    actualDuration.setText(String.format("%02d:%02d", minutes, seconds));
                    mediaPlayer.play();
                }
            });
        }
    }
   
    public void forward(ActionEvent event) {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mediaPlayer.setStartTime(mediaPlayer.getCurrentTime().multiply(1.2));
            mediaPlayer.play();
                 
            mediaPlayer.currentTimeProperty().addListener((ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) -> {
                if (mediaPlayer != null) {
                    timeBar.valueProperty().setValue(newValue.divide(musicDuration.toMillis()).toMillis() * 100.0);
                }
            });
        }
    }
    
    public void rearward(ActionEvent event) {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mediaPlayer.setStartTime(mediaPlayer.getCurrentTime().multiply(0.8));
            mediaPlayer.play();
            
            mediaPlayer.currentTimeProperty().addListener((ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) -> {
                if (mediaPlayer != null) {
                    timeBar.valueProperty().setValue(newValue.divide(musicDuration.toMillis()).toMillis() * 100.0);
                }
            });
        }
    }
    
    public void play(ActionEvent event) {   
        if (musicList.getSelectionModel().getSelectedItem() != null) {
            if (mediaPlayer != null) {
                if (mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING)) {
                    mediaPlayer.stop();
                }
            }
            String musicFile = musicList.getSelectionModel().getSelectedItem().getMusicPath();
            
            BufferedImage bufferedMusicImg = musicList.getSelectionModel().getSelectedItem().getTmpImg();
            if (bufferedMusicImg != null) {
                Image image = SwingFXUtils.toFXImage(bufferedMusicImg, null);
                atwork.setImage(image);                
            }
            else {
                atwork.setImage(new Image(getClass().getResourceAsStream("img/icon.png")));
            }
            
            sound = new Media(new File(musicFile).toURI().toString());
            mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.setVolume(soundBar.getValue());
            
            mediaPlayer.setOnReady(() -> {
                musicDuration = sound.getDuration();
                int minutes = (int) (musicDuration.toSeconds() / 60);
                int seconds = (int) (musicDuration.toSeconds() - (minutes * 60));
                totalDuration.setText(String.format("%02d:%02d", minutes, seconds));
                mediaPlayer.play();
            });
            
            mediaPlayer.currentTimeProperty().addListener((ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) -> {
                if (mediaPlayer != null) {
                    timeBar.valueProperty().setValue(newValue.divide(musicDuration.toMillis()).toMillis() * 100.0);
                    int minutes = (int) (newValue.toSeconds() / 60);
                    int seconds = (int) (newValue.toSeconds() - (minutes * 60));
                    actualDuration.setText(String.format("%02d:%02d", minutes, seconds));
                    mediaPlayer.play();
                }
            });
            
            LabelMusic.setText(musicList.getSelectionModel().getSelectedItem().getMusicName() + " - " +  musicList.getSelectionModel().getSelectedItem().getMusicAuthor());
        }
    }     
    
    public void setImgSound() {
        if (soundBar.valueProperty().getValue() < 0.25) {
                muteImg.setImage(new Image(getClass().getResourceAsStream("img/speaker0.png")));
            } else if (soundBar.valueProperty().getValue() >= 0.25 && soundBar.valueProperty().getValue() < 0.50) {
                muteImg.setImage(new Image(getClass().getResourceAsStream("img/speaker1.png")));
            } else if (soundBar.valueProperty().getValue() >= 0.50 && soundBar.valueProperty().getValue() < 0.75) {
                muteImg.setImage(new Image(getClass().getResourceAsStream("img/speaker2.png")));
            } else if (soundBar.valueProperty().getValue() >= 0.75) {
                muteImg.setImage(new Image(getClass().getResourceAsStream("img/speaker3.png")));
            }  
    }
    
    public void parameters(ActionEvent event) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Supo Player - INFO");
        alert.setHeaderText("Supo Player");
        String newLine = System.getProperty("line.separator");
        alert.setContentText("Bastien Sulyan"+newLine+"Valentin Muller"+newLine+"Gaetan Filez");
        alert.showAndWait();
    }

    @FXML
    private void dropFile(DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasFiles())
        {
            for (File file : db.getFiles())
            {
                if (file.toPath().toString().endsWith(".wav") || file.toPath().toString().endsWith(".mp3")){
                    String absolutePath = file.getAbsolutePath();
                    data = musicList.getItems();
                    Music music = new Music(file);
                    data.add(music);
                }
            }
            event.setDropCompleted(true);
        }
        else
        {
            event.setDropCompleted(false);
        }
        event.consume();
    }

    @FXML
    private void overFile(DragEvent event) {  
        Dragboard board = event.getDragboard();
        if (board.hasFiles()) {
            List<File> phil = board.getFiles();
            String path = phil.get(0).toPath().toString();

            if (path.endsWith(".mp3") || path.endsWith(".wav")) {
                event.acceptTransferModes(TransferMode.ANY);
            }        
        }
    }
}
