/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package musicplayer;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author sulyan
 */
public class Music {
    private SimpleStringProperty musicName;
    private SimpleStringProperty musicAuthor;
    private SimpleStringProperty musicTime;
    private SimpleStringProperty musicExtension;
    private SimpleStringProperty musicAlbum;
    private SimpleStringProperty musicPath;
    private SimpleStringProperty musicGenre;
    private SimpleStringProperty musicYear;    
    private BufferedImage tmpImg;

    public Music(File selectedFile) {

        String author = "";
        String album = "";
        String title = selectedFile.getName();
        String year = "";
        String genre = "";
        String time = "";

        musicPath = new SimpleStringProperty(selectedFile.getAbsolutePath());
        musicExtension = new SimpleStringProperty(selectedFile.getAbsolutePath().substring(selectedFile.getAbsolutePath().lastIndexOf(".") + 1));
            

        try {            
            if (musicExtension.get().equals("mp3")) {
                Mp3File mp3file = new Mp3File(selectedFile.getAbsolutePath());
                if (mp3file.hasId3v2Tag()) {
                    ID3v2 id3v2Tag = mp3file.getId3v2Tag();
                    author = id3v2Tag.getArtist();
                    album = id3v2Tag.getAlbum();
                    year = id3v2Tag.getYear();
                    genre = id3v2Tag.getGenreDescription();
                    
                    
                    if (!id3v2Tag.getTitle().equals(""))
                        title = id3v2Tag.getTitle();

                    
                    if (id3v2Tag.getAlbumImage() != null)
                        tmpImg = ImageIO.read(new ByteArrayInputStream(id3v2Tag.getAlbumImage()));
                    
                    int minutes = (int) (mp3file.getLengthInSeconds() / 60);
                    int seconds = (int) (mp3file.getLengthInSeconds() - (minutes * 60));

                    time = String.format("%02d:%02d", minutes, seconds);
                } 
            } else {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(selectedFile);
                AudioFormat format = audioInputStream.getFormat();
                long audioFileLength = selectedFile.length();
                int frameSize = format.getFrameSize();
                float frameRate = format.getFrameRate();
                float durationInSeconds = (audioFileLength / (frameSize * frameRate));


                int minutes = (int) (durationInSeconds / 60);
                int seconds = (int) (durationInSeconds - (minutes * 60));

                time = String.format("%02d:%02d", minutes, seconds);
            }
        } catch (IOException | UnsupportedTagException | InvalidDataException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(Music.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        musicName = new SimpleStringProperty(title);
        musicAuthor = new SimpleStringProperty(author);
        musicAlbum = new SimpleStringProperty(album);
        musicGenre = new SimpleStringProperty(genre);
        musicYear = new SimpleStringProperty(year);
        musicTime = new SimpleStringProperty(time);
    }
    
    public String getMusicName() {
        return musicName.get();
    }
    
    public void setMusicName(String s) {
        this.musicName = new SimpleStringProperty(s);
    }
    
    public String getMusicAuthor() {
        return musicAuthor.get();
    }
    
    public void setMusicAuthor(String s) {
        this.musicAuthor = new SimpleStringProperty(s);
    }
    
    public String getMusicTime() {
        return musicTime.get();
    }
     
    public void setMusicTime(String s) {
        this.musicTime = new SimpleStringProperty(s);
    }
    
    public String getMusicExtension() {
        return musicExtension.get();
    }
    
    public void setMusicExtension(String s) {
        this.musicExtension = new SimpleStringProperty(s);
    }
    
    public String getMusicAlbum() {
        return musicAlbum.get();
    }
    
    public void setMusicAlbum(String s) {
        this.musicAlbum = new SimpleStringProperty(s);
    }
    
    public String getMusicPath() {
        return musicPath.get();
    }
    
    public void setMusicPath(String s) {
        this.musicPath = new SimpleStringProperty(s);
    }
    
    public String getMusicGenre() {
        return musicGenre.get();
    }
    
    public void setMusicGenre(String s) {
        this.musicGenre = new SimpleStringProperty(s);
    }
    
    public String getMusicYear() {
        return musicYear.get();
    }
    
    public void setMusicYear(String s) {
        this.musicYear = new SimpleStringProperty(s);
    }
    
    public BufferedImage getTmpImg() {
        return this.tmpImg;
    }
}
